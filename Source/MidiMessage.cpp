//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Samuel Reis on 10/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <cmath>

MidiMessage::MidiMessage() //contructor
{
    number = 60;
    channel = 1;
    velocity = 60;
}
MidiMessage::MidiMessage(int initNumber, int initVelocity, int initChannel)
{
    number = initNumber;
    velocity = initVelocity;
    channel = initChannel;
}
MidiMessage::~MidiMessage() //Destructor
{
    
}
void MidiMessage::setNoteNumber (int value) //mutator
{
    if (value > 0 && value < 128)
    {
        number = value;
    }
}
int MidiMessage::getNoteNumber () const //accessor
{
    return number;
}
float MidiMessage::getMidiNoteinHertz() const //accessor
{
    return 440 * pow(2, (number-69) / 12.0);
}
void MidiMessage::setVelocity (int vel) //mutator
{
    if (vel > 0 && vel < 128)
    {
        velocity = vel;
    }
}
int MidiMessage::getNoteVelocity () const //accessor
{
    return velocity;
}
float MidiMessage::getFloatVelocity() const //accessor
{
    return (1/127.0 * velocity);
}
void MidiMessage::setNoteChannel(int channelNum) //mutator
{
    if (channelNum > 0 && channelNum < 128)
    {
        channel = channelNum;
    }
}
int MidiMessage::getNoteChannel() const //accessor
{
    return channel;
}