//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"


int main (int argc, const char* argv[])
{
    MidiMessage myMidiMessage1(40, 64, 2);
    
    std::cout << myMidiMessage1.getNoteNumber() << std::endl;
    std::cout << myMidiMessage1.getNoteVelocity() << std::endl;
    std::cout << myMidiMessage1.getNoteChannel() << std::endl;

    /* Text just testing commit access rights*/
    
    return 0;
}

