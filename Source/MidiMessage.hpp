//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Samuel Reis on 10/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

class MidiMessage
{
public:
    //Inititalises the Class with preset values
    MidiMessage(); //contructor

    //Initialises the class with values defined by client
    MidiMessage(int initNumber, int initVelocity, int initChannel);//constructor

    //This function can be set to do something when the object goes out of scope
    ~MidiMessage(); //Destructor

    //sets MIDI note number of the message
    void setNoteNumber(int value); //mutator
    
    //Returns the note number of the message
    int getNoteNumber() const; //accessor

    //Returns the Frequency of the note in hertz
    float getMidiNoteinHertz() const; //accessor

    //Sets the velocity of the message
    void setVelocity(int vel); //mutator

    //Returns the velocity of the message
    int getNoteVelocity() const; //accessor

    //Returns the amplitude as a float between 0 and 1
    float getFloatVelocity() const; //accessor

    //Sets the MIDI channel of the message
    void setNoteChannel(int channelNum); //mutator

    //Returns the MIDI channel of the message
    int getNoteChannel() const; //accessor

private:
    int number, velocity, channel;
};

#endif /* MidiMessage_hpp */
